import React from "react";
import Spinner from "../../components/Spinner";
import TableUser from "./TableUser";

export default function UserPage() {
  return (
    <div>
      <TableUser />
    </div>
  );
}
