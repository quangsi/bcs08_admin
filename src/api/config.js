import axios from "axios";
import { userLocalStorage } from "./localService";
import { store } from "..";
import { setLoadingOff, setLoadingOn } from "../redux/spinnerSlice";
export const TOKEN_CYBER =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCA1NiIsIkhldEhhblN0cmluZyI6IjAzLzA0LzIwMjQiLCJIZXRIYW5UaW1lIjoiMTcxMjEwMjQwMDAwMCIsIm5iZiI6MTY4MzMwNjAwMCwiZXhwIjoxNzEyMjUwMDAwfQ.YeDhc_oSixV2XtFPDzcpxFhBos5832JpQpndHNoqZLk";

export const configHeaders = () => {
  return {
    TokenCybersoft: TOKEN_CYBER,
  };
};
export const BASE_URL = "https://movienew.cybersoft.edu.vn/api";
// axios instance
export const https = axios.create({
  baseURL: BASE_URL,
  headers: {
    TokenCybersoft: TOKEN_CYBER,
    Authorization: "bearer " + userLocalStorage.get()?.accessToken,
    // accessToken
  },
});

// interceptor  : can thiệp vào request và response từ api
// xây dựng chức năng login: config 1 lần , sau đó áp dụng cho mọi api
// 1. spinnerSlice ~ giữ trạng thái bật tắt loading
// 2. dispatch => dispatch ngoài component

// Add a request interceptor
https.interceptors.request.use(
  function (config) {
    store.dispatch(setLoadingOn());
    console.log("api đi");
    return config;
  },
  function (error) {
    return Promise.reject(error);
  }
);

// Add a response interceptor
https.interceptors.response.use(
  function (response) {
    store.dispatch(setLoadingOff());
    console.log("api về");
    return response;
  },
  function (error) {
    store.dispatch(setLoadingOff());

    return Promise.reject(error);
  }
);
// load lại trang và quan sát console.log
