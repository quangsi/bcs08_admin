import axios from "axios";
import { BASE_URL, configHeaders, https } from "./config";

// export let getUserListServ = () => {
//   https.get("/QuanLyNguoiDung/LayDanhSachNguoiDung");
// };

export let userServ = {
  getList: () => {
    return https.get("/QuanLyNguoiDung/LayDanhSachNguoiDung?MaNhom=GP00");
  },
  delete: (taiKhoan) =>
    https.delete(`/QuanLyNguoiDung/XoaNguoiDung?TaiKhoan=${taiKhoan}`),
};
